import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ResultPage.scss'

export class ResultPage extends Component { 

    constructor(props) {
        super(props);
    }

    learnMore(e){

        var params = this.props.learnMoreHelper();
        
        analytics.track('Challenge Learn more', {
            ChallengeId: params.ChallengeId,
            Sponsored: params.Sponsored,
            RedirectedTo: params.RedirectedTo,
            userId: params.userId
          });

          window.open(params.RedirectedTo, '_blank');  
    
    }

    render() {
        return this.props.opened ? (
        <div className="ResultPage-container">
        <div><button className="btn btn-lg btn-success" onClick={this.props.onCloseClick}>X</button></div>
              
            <div><img src={this.props.renderImage()}></img></div>
            <div><button className="btn btn-lg btn-success" onClick={this.learnMore.bind(this)}>Learn more</button></div>
        </div>
        ) : null;
    }
}
  
ResultPage.propTypes = {
    opened: PropTypes.bool,
    onCloseClick: PropTypes.func,
    renderImage: PropTypes.func,
    learnMoreHelper: PropTypes.func
}
  
export default ResultPage