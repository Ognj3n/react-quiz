import React from 'react';
import Countdown from 'react-countdown-now';

// Random component
const Completionist = () => <span>Next question</span>;

// Renderer callback with condition
const renderer = ({ minutes, seconds, completed }) => {
  if (completed) {
    // Render a completed state
    return <Completionist />;
  } else {
    // Render a countdown
    return <span>{minutes}:{seconds}</span>;
  }
};

const CounterClockwise = (callback) => (

<Countdown date={Date.now() + 60000} renderer={renderer}>
    </Countdown>

);

export default CounterClockwise;