import * as React from 'react';
import ReactStopwatch from 'react-stopwatch';
import PropTypes from 'prop-types'
 
export const submitAnswer = () => {

}

const Stopwatch = (callback) => (

  <ReactStopwatch
    seconds={0}
    minutes={0}
    hours={0}
    limit="00:00:10"
    onCallback={() => console.log('fired')}
    render={({ formatted }) => {
      return (
        <div>
          <h3>
            Time: { formatted }
          </h3>
        </div>
      );
    }}
   />
);

Stopwatch.propTypes = {
    callback: PropTypes.func,
}
 
export default Stopwatch;