import React from 'react'
import './Header.scss'

export const Header = () => (

  <div>
    <h1 className="Header-title">Millionaire Quiz Game</h1>
  </div>
)

export default Header
