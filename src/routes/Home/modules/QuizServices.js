// fetch questions from server
export function fetchQuestionsService() {
  const URL = 'https://get-wet-api.herokuapp.com/api/randomquestions?count=15';
  //const URL = '/quizQuestions.json';
  
  return fetch(URL, { method: 'GET'})
     .then( response => Promise.all([response, response.json()]));
}

// fetch answers from server
// in this sample, the answers already included inside questions data
// for demostration purpose, so just load it again
export function submitAnswersService(state) {
  //const URL = '/quizQuestions.json';

  const URL = 'https://get-wet-api.herokuapp.com/api/checkanswer?id='+state.home.answers[0].id+'&answer='+state.home.answers[0].selected.substr(state.home.answers[0].selected.length-1);

  return fetch(URL, { method: 'GET'})
     .then( response => Promise.all([response, response.json()]));
}

// check answers on server
export function checkAnswersService(state) {
  
  const URL = 'https://get-wet-api.herokuapp.com/api/getcorrectanswer?id='+state.home.questions[0].id;

  //const URL = '/quizQuestions.json';

  return fetch(URL, { method: 'GET'})
     .then( response => Promise.all([response, response.json()]));
     

}
