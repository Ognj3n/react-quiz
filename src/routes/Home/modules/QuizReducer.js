import { fetchQuestionsService, submitAnswersService, checkAnswersService } from './QuizServices'

function fetchSuccess(payload) {
  return {
    type: 'FETCH_SUCCESS',
    payload
  }
}

function fetchError(error) {
  return {
    type: 'FETCH_ERROR',
    payload: error
  }
}

function validateAnswers(payload) {
  return {
    type: 'VALIDATE_ANSWERS',
    payload
  }
}

// export dispatch function
export function fetchQuestionsWithRedux() {
    return (dispatch, state) => {
      if (state.questions) {
        return;
      };

      return fetchQuestionsService().then(([response, json]) =>{
        
          if(response.status === 200){
            dispatch(fetchSuccess(json))
          }
        else{
          dispatch(fetchError())
        }
      })
  }
}

// export dispatch function
export function selectedAnswerAction(answers) {
    return (dispatch) => {
      dispatch({
        type: 'SELECTED_ANSWER_ACTION',
        payload: answers
      })
    }
}

// export dispatch function
export function resetQuizAction() {
    return (dispatch) => {
      dispatch({
        type: 'RESET_ANSWER_ACTION'
      })
    }
}

// export dispatch function
export function submitAnswersWithRedux() {
    return (dispatch, state) => {
      return submitAnswersService(state()).then(([response, json]) =>{
          if(response.status === 200){
          dispatch(validateAnswers(json))
        }
        else{
          dispatch(fetchError(response.error))
        }
      })
  }
}

export function checkAnswersWithRedux(){
  return (dispatch, state) => {
     checkAnswersService(state()).then(([response, json]) =>{
        if(response.status === 200){
          dispatch(fetchCorrectAnswer(json));
        }
      else{
        dispatch(fetchError(response.error))
      }
    })
}
}


function randomPickQuestions(questions, num) {

  let randomPicked = questions.sort(() => .5 - Math.random()).slice(0, num),
      answers = randomPicked.map(item => {
        return {id: item.id, selected: null};
      }); 

  return {questions: randomPicked, answers: answers}
  
}

function buildStatsState(right, wrong, total) {

  return [{
    label: 'Correct Answers',
    value: right
  }, {
    label: 'Wrong Answers',
    value:  wrong
  }, {
    label: 'Total Questions',
    value: 15
  }]
}

// export dispatch function
export function toggleModalAction(showed) {
    return (dispatch) => {
      dispatch({
        type: 'TOGGLE_MODAL_ACTION',
        payload: showed
      })
    }
}

export function fiftyFifty(reduce){
  return (dispatch,state) => {
    checkAnswersService(state()).then(([response, json]) =>{
    dispatch({
      type: 'FiFTY_FIFTY',
      payload: {reduce:reduce,json:json}
      })
    })
  }
}

export function addHelpTip(questionId){

  return (dispatch) => {
    dispatch({
      type: 'GET_THE_HINT',
      payload: questionId
    })
  }

}

export function closeResultPage(){

  return (dispatch, state) => {
    return submitAnswersService(state()).then(([response, json]) =>{
      dispatch({
        type: 'CLOSE_RESULT_PAGE',
      })
      fetchQuestionsWithRedux();
    })
  }

}

//define initialState structure to make states more clear
const initialState = {
  questions: [],
  answers: [],
  submited: false,
  showNumOfQuiz: 1,
  error: null,
  currentQuestionNum: 1,
  totalQuestionNum:15,
  correctAnswers: 0,
  wrongAnswers: 0,
  isResultPageOpened:false,
  uid: '0x10905FB47F132df47818a602C7B20BD068069065',
  userWon:null,
  challengeAnswerBool:null

}

let changedState = {...initialState};

export default function (state = initialState, action) {
  switch (action.type) {

    case 'FETCH_SUCCESS':

      state.currentQuestionNum = 1;

      state.correctAnswers = 0;

      state.wrongAnswers = 0;

      let quiz = randomPickQuestions(action.payload, state.showNumOfQuiz);

      analytics.track('Challenge Started', {
        ChallengeId: quiz.questions[0].id,
        ChallengeName: quiz.questions[0].challengeName,
        Sponsored: quiz.questions[0].sponsored,
        Uid: state.uid
      });

      return {...state, ...quiz, quizLibrary: action.payload, error: null,submited: false};

    case 'SELECTED_ANSWER_ACTION':

      let index = state.answers.findIndex(item => item.id === action.payload.id) || 0;
      let newAnswers = state.answers.slice(0);
          newAnswers[index] = action.payload;
      return {...state, answers: newAnswers};


    case 'FiFTY_FIFTY':

    console.log(action.payload);

    //remove correct answer
        
    var index = action.payload.reduce.filteredAnswers.indexOf('textAnswer' + action.payload.json[0]);
      if (index !== -1) action.payload.reduce.filteredAnswers.splice(index, 1);

      //remove random
      action.payload.reduce.filteredAnswers.splice(Math.floor(Math.random()*action.payload.reduce.filteredAnswers.length), 1);
     
    //remove two of wrong answers from all answers
    action.payload.reduce.filteredAnswers.map(item => delete state.questions[0][item]);  

    return {...state, questions:[state.questions[0]], fittyDisabled: true};

    case 'GET_THE_HINT':

    return {...state, hint:'Google it', helpLineDisabled: true};


    case 'RESET_ANSWER_ACTION':

      let resetQuiz = randomPickQuestions(state.quizLibrary, state.showNumOfQuiz);

      //changedState = {...initialState};

      return {...initialState, ...resetQuiz, quizLibrary: state.quizLibrary}

    case 'VALIDATE_ANSWERS':

    console.log('------validate answers akcija--------');

    console.log('citav state');
    
    console.log(state);

    console.log('payload akcije');

    console.log(action.payload);

    var userWon = false;
    
    console.log('redni broj pitanja na pocetku');

    console.log(state.currentQuestionNum);

    console.log('ukupan broj pitanja');

    console.log(state.totalQuestionNum);

    if(state.currentQuestionNum < state.totalQuestionNum){

      questionNum = state.currentQuestionNum++;

      console.log('question num');

      console.log(questionNum);

      console.log('broj pitanja iz stejta nakon plus plusa');

      console.log(state.currentQuestionNum);

      changedState.currentQuestionNum++;

      console.log('Changed state');

      console.log(changedState);

      console.log('state.correctAnswers');

      action.payload ? state.correctAnswers++ : state.wrongAnswers++;

      console.log(state.correctAnswers);
      
      if(state.correctAnswers==state.totalQuestionNum)
        userWon = true;

    }else
        if(state.correctAnswers==state.totalQuestionNum)
          userWon = true;
 
      state.challengeAnswerBool = action.payload;

      console.log('stejt na kraju validation akcije; on se vraca');

      console.log(state);

      console.log('------validate answers akcija KRAJ--------');

      return {...state, submited: false, isResultPageOpened:true, userWon:userWon, challengeAnswerBool:action.payload}
          
    case 'TOGGLE_MODAL_ACTION':
      return {...state, isModalOpened: action.payload};

    case 'CLOSE_RESULT_PAGE':

    console.log('------close result page akcija-------');

    console.log('cijeli stejt');

    console.log(state);

    let stats = buildStatsState(state.correctAnswers , state.wrongAnswers, state.currentQuestionNum);

    console.log('statistika');

    console.log(stats);

    let questionNum;

    var userWon = false;

    var isModalOpened = false;

    console.log('state.currentQuestionNum');

    console.log(state.currentQuestionNum);

    console.log('state.totalQuestionNum');

    console.log(state.totalQuestionNum);
    
    if(state.currentQuestionNum < state.totalQuestionNum){

      console.log('uslo ovdje');

      if(state.wrongAnswers){

        console.log('uslo ovdje pogresan odgovor');
        
        return {...state, stats, submited: true, isModalOpened: true,isResultPageOpened:false, userWon: userWon};
        
      }

      console.log('tacan odgovor');

      console.log('quiz library prije uklanjanja proslog pitanja');

      console.log(state.quizLibrary);

      state.quizLibrary.splice(0,1);

      console.log('quiz library nakon uklanjanja proslog pitanja; ovo ide u random pick questions');

      console.log(state.quizLibrary);

      let quiz = randomPickQuestions(state.quizLibrary, state.showNumOfQuiz);

      console.log('novo pitanje');

      console.log(quiz);
      
      if(state.correctAnswers==state.totalQuestionNum){
        console.log('uslo nemoguce logicki');
        userWon = true;
        isModalOpened = true;
      }
        
      state.userWon = userWon;

      console.log('------close result page akcija-------');

      return {...state,...quiz, submited: false , questionNum: questionNum,isModalOpened: false, isResultPageOpened:false}
       

    }else{
      console.log('uslo zadnje pitanje');

      if(state.correctAnswers==state.totalQuestionNum)
        userWon = true;

      questionNum = state.totalQuestionNum;
      
      return {...state, stats, submited: false, isModalOpened: true, isResultPageOpened:false,userWon: userWon}

    }

    case 'FETCH_ERROR':
      return {...state, error:action.payload}
    default:
      return state;
  }
}
