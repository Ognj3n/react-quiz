import React, { Component } from 'react'
import QuizCard from '../../../components/QuizCard'
import ModalBox from '../../../components/ModalBox'
import ResultPage from '../../../components/ResultPage';
import io from 'socket.io-client';
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import "react-notifications-component/dist/theme.css";
import './HomeView.scss'

export class HomeView extends Component {

  constructor() {
    super()
    this.state = {
                  enableSubmit: false, 
                  fiftyDisabled: false,
                  helpLineDisabled: false,
                  helpLineShown:true,
                  userId: '0x10905FB47F132df47818a602C7B20BD068069065',
                  userWon: false
                };
                
    const socket = io('https://thawing-hollows-66005.herokuapp.com'); 
             
    socket.on(this.state.userId, (result) => {

        console.log(result)

          toast.success(result.message + "\n\r" +
            result.award+" "+result.symbol, {
              position: toast.POSITION.TOP_RIGHT
          });
              
    })        

  }


  //make sure of uniqueness of questions
  //test user won  
  // correct answer ++  at the end
  componentDidMount(){
    
    this.props.fetchData();
    
  }

  componentWillReceiveProps(props) {
  
    console.log(this.props);

    let answers = props.answers,
        inComplete = answers && answers.some(item => !item.selected);   

    if(this.props.userWon!=null && this.props.userWon!=undefined)  
      this.setState({userWon: this.props.userWon});
        
    this.setState({enableSubmit: !inComplete});
    
  }

  computeSubmitButtonState(answers) {
    console.log(answers);
    return answers.some(item => item.answer);
  }

  selectAnswer(questionId, selectedAnswer) {
    this.props.selectedAnswer({id:questionId, selected:selectedAnswer});
  }

  closeModal(e) {
    this.props.toggleModalAction(false);
    this.props.fetchData();
    //this.playAgain();
  }

  closeResultPage(e){

    this.state.helpLineShown = true; 
    this.props.closeResultPage();
    analytics.track('Challenge Completed',{
      ChallengeId: this.props.questions[0].id,
      userId: this.state.userId
    });
    
  }

  playAgain(){

    //ovdje dodati i provjeru da li je kviz gotov i provjeriti da li se quiz library mijenja
    if(this.props.challengeAnswerBool)
    this.props.resetQuizAction();
    else
    this.props.fetchData();
    

    this.state.fiftyDisabled = false;
    this.state.helpLineDisabled = false; 
    this.state.helpLineShown = true; 

  }

  submitAnswers (e) {

    this.props.submitAnswers(e);

    analytics.track({
      event : 'Challenge Answer Submitted', 
      ChallengeId: this.props.questions[0].id,
      ChallengeName: this.props.questions[0].challengeName,
      Sponsored: this.props.questions[0].sponsored,
      FiftyFiftyUsed: this.state.fiftyDisabled,
      HelpLineUsed: this.state.helpLineDisabled,
      SelectedAnswer: this.props.answers[0].selected.substr(this.props.answers[0].selected.length - 1),
      CorrectAnswer: this.props.questions[0].correctAnswer,
      UserWon: this.state.userWon,
      userId: this.state.userId
    });

  }

  fiftyFifty(e){    

     var filteredAnswers = Object.keys(this.props.questions[0]).filter(item => item !== 'id' && item !== 'sponsored' && item !== 'challengeId' && item !== 'imageQuestionUrl' && item !== 'eventChallengeName' && item !== 'textHint' && item != 'correctAnswer' && item != 'imageCorrectAnswerUrl' && item != 'imageWrongAnswerUrl' && item != 'learnMoreUrl');

     this.props.fiftyFifty({filteredAnswers:filteredAnswers,id:this.props.questions[0].id});

     this.state.fiftyDisabled = true;
  
  }


  //adds a help tip
  addHelpTip(e){

    this.props.addHelpTip(this.props.questions[0].id);

    this.setState({helpLineDisabled: true});

    this.setState({helpLineShown: false});

  }
  
  putResultPageImage(e){

    if(this.props.challengeAnswerBool!=null)     
    return this.props.challengeAnswerBool ? this.props.questions[0].imageCorrectAnswerUrl : this.props.questions[0].imageWrongAnswerUrl;
  
  }

  spitHint(e){
  
    return this.props.questions.length ? this.props.questions[0].textHint:null;

  }

  passDataToResultPage(e){

    var data = {};

    data.ChallengeId = this.props.questions[0].id;
    data.Sponsored = this.props.questions[0].sponsored;
    data.RedirectedTo = this.props.questions[0].learnMoreUrl;
    data.userId = this.state.userId;

    return data;

  }


// Render Quiz Cards
  renderQuziCards() {

      return this.props.questions && this.props.questions.map((item, index)=> {

        return (<QuizCard key={index}
                    item={item}
                    onChange={this.selectAnswer.bind(this)}
                    selected={this.props.answers && this.props.answers[index] && this.props.answers[index].selected}
                    showCorrectAnswer={this.props.submited}></QuizCard>)
      });

  }

// Render Header Part
  renderHeader() {
    return (<div className="HomeView-header">
    <div className="HomeView-buttons-group">
        <button className="btn btn-lg btn-success" onClick={this.fiftyFifty.bind(this)} disabled={this.state.fiftyDisabled}>50:50</button>
        <button className="btn btn-lg btn-success" onClick={this.addHelpTip.bind(this)} disabled={this.state.helpLineDisabled}>Help Line</button>
    </div>
    <div hidden={this.state.helpLineShown}>{this.spitHint()}</div>
    </div>);
  }

  
  render() {
    return (<div className="HomeView-container">
        { this.renderHeader() }
        { this.renderQuziCards() }
        <div className="HomeView-buttons-group">
          <button className="btn btn-lg btn-success"
                  type="button"
                  disabled={ !this.state.enableSubmit && !this.props.submited }
                  onClick={ this.submitAnswers.bind(this) }>Submit</button>
          <button className="btn btn-lg btn-inverse"
                  type="button"
                  onClick={ this.playAgain.bind(this) }>Play again</button>
        </div>
        { this.props.error ? (<h6 className="HomeView-error">this.props.error</h6>) : null}
        <ModalBox title="Your Status:" opened={ this.props.isModalOpened } onCloseClick={ this.closeModal.bind(this) }>
        {
            this.props.stats && Object.keys(this.props.stats).map((key, index) => {
                return (
                  <h6 key={index}> { this.props.stats[key].label + " : " + this.props.stats[key].value } </h6>
                )
            })
        }
        </ModalBox>
        <ResultPage opened={ this.props.isResultPageOpened } onCloseClick={ this.closeResultPage.bind(this) } renderImage={this.putResultPageImage.bind(this)} learnMoreHelper={this.passDataToResultPage.bind(this)}>
        </ResultPage>
        <ToastContainer />
    </div>);
  }
}

export default HomeView
