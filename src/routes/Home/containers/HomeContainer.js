import { connect } from 'react-redux'
import HomeView from '../components/HomeView'
import {
  fetchQuestionsWithRedux,
  selectedAnswerAction,
  resetQuizAction,
  submitAnswersWithRedux,
  toggleModalAction,
  fiftyFifty,
  addHelpTip,
  closeResultPage,
  checkAnswersWithRedux
} from '../modules/QuizReducer'

// map dispath function from QuizReducer
const mapDispatchToProps = {
  fetchData: fetchQuestionsWithRedux,
  selectedAnswer: selectedAnswerAction,
  resetQuizAction: resetQuizAction,
  submitAnswers: submitAnswersWithRedux,
  checkAnswersService: checkAnswersWithRedux,
  toggleModalAction: toggleModalAction,
  fiftyFifty:fiftyFifty,
  addHelpTip:addHelpTip,
  closeResultPage:closeResultPage
}

// map state to props from global state.home
const mapStateToProps = (state) => ({
  questions: state.home.questions,
  answers: state.home.answers,
  submited: state.home.submited,
  stats: state.home.stats,
  isModalOpened: state.home.isModalOpened,
  quizLibrary: state.home.quizLibrary,
  error: state.home.error,
  isResultPageOpened: state.home.isResultPageOpened,
  userWon: state.home.userWon,
  challengeAnswerBool: state.home.challengeAnswerBool
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)
