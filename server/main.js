const express = require('express')
const debug = require('debug')('app:server')
const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('../config/webpack.config')
const project = require('../config/project.config')
const compress = require('compression')

const app = express()
var http = require('http').Server(app);

// Apply gzip compression
//change challenge id into question text
//okacit api na swagger
//internal mongo_id, another id

app.use(compress())
const port = process.env.PORT || 3000;

http.listen(port, function(){
  console.log('Server started, listening on port: ' + port);
});

module.exports = app
